#!/bin/bash
cd /etc/uwsgi/vassals/
ln -is /var/www/django/PROJ_NAME/PROJ_NAME/deploy/PROJ_NAME.ini
sudo service uwsgi reload

cd /etc/nginx/sites-enabled
ln -is /var/www/django/PROJ_NAME/PROJ_NAME/deploy/PROJ_NAME.conf
sudo nginx -t
echo 'reload nginx if test pass:'
echo 'sudo service nginx reload'

