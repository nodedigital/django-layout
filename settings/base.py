import sys
from path import path
from os.path import dirname,abspath
HERE = path(dirname(abspath(__file__)))
PROJ_ROOT = BASE_DIR = HERE.parent
PROJ_NAME = PROJ_ROOT.name
APPS_ROOT = PROJ_ROOT/'apps'

WWW_ROOT  = PROJ_ROOT.parent.parent/'www'
STATIC_ROOT = WWW_ROOT/'static'
MEDIA_ROOT = WWW_ROOT/'media'
CACHE_ROOT = WWW_ROOT/'cache'

sys.path.insert(0,PROJ_ROOT)
sys.path.insert(0,APPS_ROOT)

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '@w@4gg7^dy9z3(r%n0b&*rr)4cf0-b$=hpsyx5qo0r4l3b=i$8'

ALLOWED_HOSTS = ['*']

ROOT_URLCONF = 'urls'

WSGI_APPLICATION = 'wsgi.application'

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Pacific/Auckland'
USE_TZ = True

USE_I18N = True
USE_L10N = True

STATIC_URL = '/static/'
MEDIA_URL  = '/media/'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': PROJ_ROOT/'db.sqlite'
    }
}

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.sites',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.flatpages',

    'annoying',
    'django_extensions',
    'crispy_forms',
    'avatar',
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    #'allauth.socialaccount.providers.facebook',

    'main',
)

STATICFILES_DIRS = (
    PROJ_ROOT/"static",
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

STATICFILES_STORAGE = 'django.contrib.staticfiles.storage.CachedStaticFilesStorage'

TEMPLATE_DIRS = (
    PROJ_ROOT/"templates",
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.request",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.tz",
    "django.contrib.messages.context_processors.messages",
    "allauth.account.context_processors.account",
    "allauth.socialaccount.context_processors.socialaccount",
    "main.context_processors.g",
)

FIXTURE_DIRS = (
    PROJ_ROOT/"fixtures",
)

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    "allauth.account.auth_backends.AuthenticationBackend",
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

EMAIL_HOST='smtp.gmail.com'
SERVER_EMAIL = DEFAULT_FROM_EMAIL = EMAIL_HOST_USER = 'node@node.co.nz'
EMAIL_HOST_PASSWORD='override-in-local.py'
EMAIL_SUBJECT_PREFIX='[%s]' % PROJ_NAME
EMAIL_USE_TLS=True
EMAIL_PORT=587

SITE_ID = 1
SITE_NAME = PROJ_NAME
SITE_DOMAIN = '%s.co.nz' % SITE_NAME
SITE_PROTOCOL = 'https'
SITE_URL = '%s://%s' % (SITE_PROTOCOL, SITE_DOMAIN)

# django-crispy-form
CRISPY_TEMPLATE_PACK = 'bootstrap3'

# django-allauth
ACCOUNT_AUTHENTICATION_METHOD = 'email' # email|username|username_email
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_EMAIL_VERIFICATION = 'mandatory' # optional|mandatory|none
ACCOUNT_DEFAULT_HTTP_PROTOCOL = 'https'
ACCOUNT_USERNAME_REQUIRED = False
ACCOUNT_LOGOUT_ON_GET = True
ACCOUNT_EMAIL_CONFIRMATION_AUTHENTICATED_REDIRECT_URL = '/'
ACCOUNT_SESSION_REMEMBER = True # enable remember me checkbox

