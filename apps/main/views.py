#!/usr/bin/env python
# encoding: utf-8
from __future__ import unicode_literals
from path import path
from os.path import abspath,dirname
#from django.shortcuts import redirect
#from django.contrib.auth.models import User
from annoying.decorators import render_to
#from . import models as m
#from . import forms  as f
import logging
logger = logging.getLogger(__name__)

APP_ROOT = path(dirname(abspath(__file__)))
APP_NAME = APP_ROOT.name

def T(name, ext='html'):
    return '{}/{}.{}'.format(APP_NAME, name, ext)

@render_to('home.html')
def home(request):
    ctx = {}
    logger.info('welcome home!')
    return ctx
