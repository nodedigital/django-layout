#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.template import Library
from django.template.defaultfilters import stringfilter

register = Library()

################ Filters
@register.filter
def filter_with_arg(value, arg):
    return value

@register.filter
@stringfilter
def string_filter(s):
    return s

################ Tags
@register.simple_tag()
def simple_tag(arg1, arg2, arg3):
    return ''

@register.simple_tag(takes_context=True)
def tag_with_context(context, pk):
    user = context['user']
    return user

@register.inclusion_tag('path/to/template.html')
def sidebar(obj):
    return {'obj': obj}
