#!/usr/bin/env python
# encoding: utf-8
from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User
from annoying.fields import AutoOneToOneField

class Profile(models.Model):
    user = AutoOneToOneField(User)
    dob = models.DateField(blank=True, null=True)
    # add more fileds here...
