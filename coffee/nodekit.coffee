class NodeKit
  constructor: ->
    @_csrfAjaxPatch()
    @removeMobileHover()
    # $('input, textarea').placeholder()
    # will cause the NZPost selection doesn't work on mobile
    # FastClick.attach(document.body)
    $(document).foundation()

  isSmallScreen: ->
    $(window).width() <= 640

  isHighDensity: ->
    if window.matchMedia?
      window.matchMedia('only screen and (min-resolution: 124dpi), only screen and (min-resolution: 1.3dppx), only screen and (min-resolution: 48.8dpcm)').matches \
      or window.matchMedia('only screen and (-webkit-min-device-pixel-ratio: 1.3), only screen and (-o-min-device-pixel-ratio: 2.6/2), only screen and (min--moz-device-pixel-ratio: 1.3), only screen and (min-device-pixel-ratio: 1.3)').matches
    else if window.devicePixelRatio?
      window.devicePixelRatio > 1.3

  # disable :hover on touch devices
  # https://gist.github.com/javan/4404503
  removeMobileHover: ->
    if 'createTouch' of document
      ignore = /:hover\b/
      try
        for stylesheet in document.styleSheets
          idxs = []
          # detect hover rules
          for rule, idx in stylesheet.cssRules
            if rule.type is CSSRule.STYLE_RULE and ignore.test(rule.selectorText)
              idxs.unshift idx

          # delete hover rules
          stylesheet.deleteRule idx for idx in idxs


  _csrfAjaxPatch: ->
    # these HTTP methods do not require CSRF protection
    _csrfSafeMethod = (method) ->
      method in ['GET', 'HEAD', 'OPTIONS', 'TRACE']

    $.ajaxSetup
      beforeSend: (xhr, settings) ->
        csrftoken = $.cookie('csrftoken')
        if not _csrfSafeMethod(settings.type) and not this.crossDomain
          xhr.setRequestHeader "X-CSRFToken", csrftoken


$ ->
  window.nodekit = new NodeKit
