'use strict';

/* eslint-env node */
/* eslint quotes: [0] */

// Include gulp
var path = require('path');
var gulp = require('gulp');
var gutil = require('gulp-util');
var bg = require("gulp-bg");

// Include Our Plugins
var coffee = require('gulp-coffee');
var sass = require('gulp-sass');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var minifyCSS = require('gulp-minify-css');
var sourcemaps = require('gulp-sourcemaps');
var please = require('gulp-pleeease');

var paths = {
    bower: 'static/bower_components',
    scss: 'scss/**/*.scss',
    coffee: 'coffee/*.coffee',
    build: 'static/build'
};

// Compile Our Sass
gulp.task('sass', function() {
    return gulp.src(paths.scss)
        .pipe(sourcemaps.init())
        .pipe(sass({
            includePaths: [paths.bower]
        }))
        .pipe(please({
            minifier: false
        }))
        .pipe(minifyCSS())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(sourcemaps.write('./'))
        .on('error', gutil.log)
        .pipe(gulp.dest(paths.build));
});

//coffee
gulp.task('coffee', function() {
    gulp.src(paths.coffee)
        .pipe(sourcemaps.init())
        .pipe(coffee({
            bare: true
        }).on('error', gutil.log))
        .pipe(uglify())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(paths.build));
});

// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch(paths.scss, ['sass']);
    gulp.watch(paths.coffee, ['coffee']);
});

var manage = path.join(process.cwd(), "manage.py");

gulp.task("server", bg("python", manage, "runserver", "0.0.0.0:8000"));

// Default Task
gulp.task('default', ['sass', 'coffee', 'watch', 'server']);
