#!/usr/bin/env python
# encoding: utf-8
from __future__ import unicode_literals
from fabric.api import cd, run, env, prefix, local, hosts, prompt
from path import path
from os.path import dirname, abspath
from contextlib import contextmanager
HERE = path(dirname(abspath(__file__)))
PROJ_NAME = HERE.name
WORKON_HOME = path('/var/www/django')
VENV_ROOT = WORKON_HOME/PROJ_NAME
PROJ_ROOT = VENV_ROOT/PROJ_NAME
BIN = VENV_ROOT/'bin'
ACTIVATE = BIN/'activate'

env.use_ssh_config = True
env.hosts = ['nodeweb']
env.activate = 'source %s' % ACTIVATE
env.deploy_dir = PROJ_ROOT

@contextmanager
def virtualenv():
    with prefix(env.activate):
        yield

def tail():
    run("tail -f /var/log/uwsgi/%s.log" % PROJ_NAME)

def touch():
    with cd(env.deploy_dir):
        run("touch deploy/*.ini")
    tail()

def static():
    with cd(env.deploy_dir):
        run('bower update')
        run('mkdir -p static/build')
        run('gulp sass coffee')
        with virtualenv():
            # run('python manage.py compilestatic')
            run('python manage.py collectstatic --noinput -i node_modules')

def pip():
    with cd(env.deploy_dir):
        with virtualenv():
            run('pip install -r requirements.txt')

def syncdb():
    with cd(env.deploy_dir):
        with virtualenv():
            run('python manage.py syncdb')
            run('python manage.py migrate')

@hosts('nodedb')
def createdb():
    pswd = prompt('please input password for database %s:' % PROJ_NAME)
    run('create-databash.sh %s %s' % (PROJ_NAME, pswd))

def mkvenv():
    with cd(WORKON_HOME):
        run('virtualenv %s' % PROJ_NAME)

def gitclone():
    with cd(VENV_ROOT):
        run('git@bitbucket.org:nodedigital/%s.git' % PROJ_NAME)

def mkdirs():
    with cd(VENV_ROOT):
        run('mkdir -p public-www/static')
        run('mkdir -p public-www/media')
        run('mkdir -p public-www/cache')
        run('chmod -R 777 public-www')

def deploy():
    createdb()
    mkvenv()
    gitclone()
    pip()
    syncdb()
    mkdirs()
    static()

def pull():
    local('git push')
    with cd(env.deploy_dir):
        run("git pull")
    static()

def all():
    pull()
    touch()
