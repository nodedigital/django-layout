#!/usr/bin/env python
# encoding: utf-8
from __future__ import unicode_literals
from django.conf import settings
from django.shortcuts import render
from django.conf.urls.static import static
from django.conf.urls import patterns, include, url
from django.contrib import admin
admin.autodiscover()

def robots(request):
    t = 'robots_debug.txt' if settings.DEBUG else 'robots.txt'
    return render(request, t, content_type="text/plain")

urlpatterns = patterns('',
    url(r'^robots.txt$', robots, name='robots'),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^avatar/', include('avatar.urls')),
    #url(r'^pages/', include('django.contrib.flatpages.urls')),
)

urlpatterns += patterns('django.contrib.flatpages.views',
    url(r'^about/$', 'flatpage', {'url': '/about/'}, name='about'),
    url(r'^faq/$', 'flatpage', {'url': '/faq/'}, name='faq'),
)

urlpatterns += patterns('main.views',
    url(r'^$', 'home', name='home'),
)

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL,  document_root=settings.MEDIA_ROOT)
