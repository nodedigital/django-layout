#!/usr/bin/env python
# -*- coding: UTF-8 -*-

from plan import Plan
from path import path
from os.path import dirname, abspath
HERE = path(dirname(abspath(__file__)))
PROJ_NAME = HERE.name
SRV = path('/var/www/django')
ENV = SRV/PROJ_NAME
SRC = ENV/PROJ_NAME
WWW = ENV/'public-www'

PYTHON = ENV/'bin'/'python'
MANAGE = SRC/'manage.py'

def manage(cmd):
    return '%s %s %s' % (PYTHON, MANAGE, cmd)

cron = Plan()
cron.command(manage('daily'), every='1.day', at='0:0')

if __name__ == '__main__':
    import sys
    argv = sys.argv
    mode = argv[1] if len(argv) > 1 else 'check'
    cron.run(mode)
    print 'run following command to write the crontab file:'
    print './schedule.py write'
