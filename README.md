## About
A great, flat, reuseable, portable django layout template project.
This is intended for single sites only if you want multi site support please use the default layout.

## Quickstart
Make sure you have virtualenv and virtualenvwrapper installed.

```bash
mkvirtualenv ENV
```

First, clone the project to your machine:

```bash
git clone git@bitbucket.org:nodedigital/django-layout.git
```

Then, use this layout as a template to start your project:

```bash
django-admin.py startproject --template=django-layout PROJECT_NAME
```

or just clone:

```bash
git clone django-layout PROJ_NAME
```

## Node deploy checklist
* create database on nodedb
* create public-www and static|media|cache dirs, chown and chmod
* mkvirtualenv and setvirtualenvproject
* git clone
* change settings
* syncdb, migrate, loaddata
* nginx and uwsgi
* change uwsgi log permission
